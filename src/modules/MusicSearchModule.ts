import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { Album } from '@/models/Album';
import { musicSearch } from '../services/MusicSearchService';

@Module({})
export default class MusicSearchModule extends VuexModule {
    results: Album[] = []
    albums_query = ''
    isLoading = false
    error: string = ''

    @Action({})
    searchAlbums(query: string) {
        const { commit } = this.context
        commit('SET_ERROR', '')
        commit('SET_LOADING', true)
        commit('SET_QUERY', query)

        musicSearch.searchAlbums(this.albums_query)
            .then(data => commit('SET_RESULTS', data))
            .catch(error => commit('SET_ERROR', error.message))
            .finally(() => commit('SET_LOADING', false))
    }

    get music_query() { return this.albums_query }
    get albums() { return this.results }
    get message() { return this.error }


    @Mutation
    SET_ERROR(error: string) {
        this.error = error
    }

    @Mutation
    SET_LOADING(isLoading: boolean) {
        this.isLoading = isLoading
    }

    @Mutation
    SET_RESULTS(results: Album[]) {
        this.results = results
    }

    @Mutation
    SET_QUERY(query: string) {
        this.albums_query = query
    }
}