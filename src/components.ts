import Vue from 'vue';
import Card from './components/Card.vue';
import FormField from './components/FormField.vue';

// components.ts
// Vue.component('ItemsList',ItemsList)
Vue.component('Card', Card);
Vue.component('FormField', FormField);
