// https://codepen.io/anon/pen/ydjJPx

export class AuthService {
    constructor(
        private auth_url: string,
        private client_id: string,
        private response_type: string,
        private redirect_uri: string
    ) {
        const JSONToken = sessionStorage.getItem('token')
        if (JSONToken) {
            this.token = JSON.parse(JSONToken)
        }

        if (document.location.hash) {
            const u = new URLSearchParams(document.location.hash)
            const token = u.get('#access_token')
            if (token) {
                this.token = token
                sessionStorage.setItem('token', JSON.stringify(this.token))
                document.location.hash = ''
            }
        }
    }

    token: string | null = null;

    authorize() {
        const url = `${this.auth_url}?client_id=${this.client_id}`
            + `&response_type=${this.response_type}`
            + `&redirect_uri=${this.redirect_uri}`

        document.location.href = (url)
    }

    getToken() {
        if (!this.token) {
            this.authorize()
        }
        return this.token
    }
}

export const authService = new AuthService(
    'https://accounts.spotify.com/authorize',
    '70599ee5812a4a16abd861625a38f5a6',
    'token',
    'http://localhost:8080/'
)